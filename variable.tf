variable "region" {
  default     = ["ap-south-1", "ap-south-1a"]
  description = "AWS region and availability zone"
}

variable "cidr_block" {
  default     = ["10.0.0.0/16", "10.0.1.0/24", "0.0.0.0/0", "1.2.3.4/32"]
  description = "CIDR blocks for VPC, subnet and security group"
}

variable "private_ips" {
  default     = "10.0.1.50"
  description = "Private IP for EC2 nistance"
}

variable "key" {
  default     = "ssh-key"
  description = "SSH-Key for EC2 instance"
}

variable "type" {
  default     = "t2.micro"
  description = "Instance type of EC2 instance"
}

variable "server_name" {
  default     = "Nginx Web Server"
  description = "Name of the server"
}

variable "root_size" {
  default     = 9
  description = "Size of the root volume"

}